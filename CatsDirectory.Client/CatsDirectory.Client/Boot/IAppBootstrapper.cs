﻿namespace CatsDirectory.Client.Boot
{
    public interface IAppBootstrapper
    {
        #region Methods
        void RegisterTypes();
        #endregion
    }
}