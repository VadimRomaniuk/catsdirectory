﻿using CatsDirectory.Client.Infrastructure.Interfaces.Views;
using System.Windows.Controls;

namespace CatsDirectory.Client.Views
{
    /// <summary>
    /// Логика взаимодействия для BreedDetailsView.xaml
    /// </summary>
    public partial class BreedDetailsView : UserControl, IBreedDetailsView
    {
        public BreedDetailsView()
        {
            InitializeComponent();
        }
    }
}