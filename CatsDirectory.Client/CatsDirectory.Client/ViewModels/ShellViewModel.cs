﻿using CatsDirectory.Client.Infrastructure.Constants;
using CatsDirectory.Client.Infrastructure.Interfaces.ViewModels;
using CatsDirectory.Client.Infrastructure.Interfaces.Views;
using Prism.Regions;

namespace CatsDirectory.Client.ViewModels
{
    public class ShellViewModel : IShellViewModel
    {

        #region ctor
        public ShellViewModel(IRegionManager regionManager)
        {
            regionManager.RegisterViewWithRegion(RegionNamesConstants.MainRegion, typeof(IMainView));
        }
        #endregion

    }
}