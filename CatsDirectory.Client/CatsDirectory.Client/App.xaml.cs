﻿using CatsDirectory.Client.Boot;
using CatsDirectory.Client.Infrastructure.Interfaces.ViewModels;
using CatsDirectory.Client.Infrastructure.Interfaces.Views;
using CatsDirectory.Client.ViewModels;
using CatsDirectory.Client.Views;
using Prism.Ioc;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CatsDirectory.Client
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        #region Fields
        private IAppBootstrapper _appBootstrapper;
        #endregion

        #region OverrideMethods
        protected override Window CreateShell()
        {
            var shell = Container.Resolve<IShellView>();
            return (ShellView)shell;
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            RegisterViews(containerRegistry);
            RegisterViewModels(containerRegistry);
            //RegisterServices(containerRegistry);

        }
        #endregion

        #region Methods
        private void RegisterServices(IContainerRegistry containerRegistry)
        {
            
        }

        private void RegisterViewModels(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IShellViewModel, ShellViewModel>();
            containerRegistry.RegisterSingleton<IMainViewModel, MainViewModel>();
            containerRegistry.RegisterSingleton<IBreedDetailsViewModel, BreedDetailsViewModel>();
        }

        private void RegisterViews(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IShellView, ShellView>();
            containerRegistry.RegisterSingleton<IMainView, MainView>();
            containerRegistry.RegisterSingleton<IBreedDetailsView, BreedDetailsView>();
        }
        #endregion
    }
}